<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $title ?><?php if (!empty($sub_title)) { printf(" - %s", $sub_title); } ?></title>
        <link href="/css/bootstrap.min.css" rel="stylesheet" />
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <?php foreach ($css as $path) { ?>
        <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>">
        <?php } ?>
        <?php foreach ($js as $path) { ?>
        <script type="text/javascript" src="<?php echo $path; ?>"></script>
        <?php } ?>
    </head>
    <body>
        <?php echo $contents; ?>
    </body>
</html>
