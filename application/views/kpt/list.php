<?php foreach ($list as $item) { ?>
<li class="list-group-item">
    <?php if ($user_id == $item['author_id']) { ?>
    <a class="btn-delete glyphicon glyphicon-remove" aria-hidden="true" post-id="<?php echo $item['id'] ?>" href="#"></a>
    <?php } ?>
    <p class="text"><?php echo nl2br(htmlspecialchars($item['comment'])); ?></p>
    <div class="author"><?php echo htmlspecialchars($item['author_name']); ?></div>
</li>
<?php } ?>
