<section>
    <header>
        <div class="row">
            <div class="project col-xs-1"><?php echo htmlspecialchars($project_name); ?></div>
            <div class="col-xs-8">
                <h1 class="title"><?php echo htmlspecialchars($title); ?></h1>
                <!-- <p class="description"><?php echo nl2br(htmlspecialchars($description)); ?></p> -->
            </div>
            <div class="date col-xs-3"><?php echo date("Y/m/d", $datetime); ?></div>
        </div>
    </header>
    <div class="contents row">
        <div class="col-xs-12">
            <h2 class="title">Keep</h2>
            <?php if (!empty($keep)) { ?>
            <table class="table table-striped table-condensed">
                <tbody>
                    <?php foreach ($keep as $post) { ?>
                    <tr>
                        <td class="author"><?php echo htmlspecialchars($post['author_name']); ?></td>
                        <td class="comment"><?php echo nl2br(htmlspecialchars($post['comment'])); ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php } ?>
        </div>
    </div>
    <div class="contents row">
        <div class="col-xs-12">
            <h2 class="title">Problem</h2>
            <?php if (!empty($problem)) { ?>
            <table class="table table-striped table-condensed">
                <tbody>
                    <?php foreach ($problem as $post) { ?>
                    <tr>
                        <td class="author"><?php echo htmlspecialchars($post['author_name']); ?></td>
                        <td class="comment"><?php echo nl2br(htmlspecialchars($post['comment'])); ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php } ?>
        </div>
    </div>
    <div class="contents row">
        <div class="col-xs-12">
            <h2 class="title">Try</h2>
            <?php if (!empty($try)) { ?>
            <table class="table table-striped table-condensed">
                <tbody>
                    <?php foreach ($try as $post) { ?>
                    <tr>
                        <td class="author"><?php echo htmlspecialchars($post['author_name']); ?></td>
                        <td class="comment"><?php echo nl2br(htmlspecialchars($post['comment'])); ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php } ?>
        </div>
    </div>
</section>
<script>
    window.print();
</script>
