<?php
class MY_Model extends CI_Model
{
    protected $table_name = null;
    protected $db_loaded = false;

    public function __construct()
    {
        parent::__construct();

        if (!empty($this->table_name)) {
            $this->load->database();
            $this->db_loaded = true;
        }
    }

    public function __call($name, $args)
    {
        if ($this->db_loaded && method_exists($this->db, $name)) {
            $set_table_name_methods = [
                "get",
                "get_where",
                "insert",
                "update",
                "delete",
                'count_all',
                'count_all_results'
            ];

            if (in_array($name, $set_table_name_methods)) {
                array_unshift($args, $this->table_name);
            }

            return call_user_func_array([$this->db, $name], $args);
        }
    }
}
