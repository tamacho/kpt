<?php
class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        // template load
        $segment = $this->uri->rsegment_array();
        $vars['title'] = "KPT";

        $vars['css'] = ["/css/common.css"];
        $css = sprintf("/css/%s/%s.css", $segment[1], $segment[2]);

        if (file_exists(FCPATH . $css)) {
            $vars['css'][] = $css;
        }

        $vars['js'] = [];
        $js = sprintf("/js/%s/%s.js", $segment[1], $segment[2]);

        if (file_exists(FCPATH . $js)) {
            $vars['js'][] = $js;
        }

        $this->load->template("template", $vars);

        $this->load->helper("output");
        // $this->output->enable_profiler(true);
    }
}
