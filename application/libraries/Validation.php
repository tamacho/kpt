<?php
require_once APPPATH . "core/BaseLibrary.php";
require_once APPPATH . "libraries/exceptions/ValidationErrorException.php";

class Validation extends BaseLibrary
{
    private $last_error_message = null;

    private $last_error_code = null;

    /**
     * validation check
     */
    public function check($data, $message = null, $code = 0, $throw = true)
    {
        $result = true;

        try {
            if (is_callable($data)) {
                $args = array_slice(func_get_args(), 1, -2);

                $result = (boolean) call_user_func_array($data, $args);
            } else {
                $result = (boolean) $data;
            }
        } catch (Exception $ex) {
            $result = false;
        }

        if (!$result) {
            $this->last_error_message = $message;
            $this->last_error_code = $code;

            if ($throw) {
                throw new ValidationErrorException($message, $code);
            }
        }

        return $result;
    }

    /**
     * validation required check
     */
    public function required($data, $message, $code = 0, $throw = true)
    {
        $result = false;

        if (isset($data)) {

            if (is_array($data)) {
                $data = implode("", $data);
            }

            $result = "" !== trim($data);
        }

        return $this->check($result, $message, $code, $throw);
    }

    /**
     * validation id check
     */
    public function id($data, $id_name = "id", $message = null, $code = 0, $throw = true)
    {
        $required_message = null;
        $numeric_message = null;

        if (empty($message)) {
            $required_message = sprintf("%s is required.", $id_name);
            $numeric_message = sprintf("%s is numeric.", $id_name);
        } else {
            $required_message = $message;
            $numeric_message = $message;
        }

        $result = $this->required($data, $required_message, $code, $throw);

        if ($result) {
            $result = is_numeric($data) && preg_match("/^[1-9][0-9]*$/", $data);
            $this->check($result, $numeric_message, $code, $throw);
        }

        return $result;
    }

    /**
     * validation check failed
     */
    public function failed($message, $code = 0, $throw = true) {
        return $this->check(false, $message, $code, $throw);
    }

    public function last_error_message()
    {
        return $this->last_error_message;
    }
}
