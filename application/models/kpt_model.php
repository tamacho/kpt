<?php
class Kpt_model extends MY_Model
{
    protected $table_name = "kpt";

    public function getById($id)
    {
        $result = null;

        $query = $this->select([
            "kpt.id",
            "kpt.project_id",
            "project.name as project_name",
            "kpt.title",
            "kpt.kpt_datetime",
            "kpt.description"
        ])->from("kpt")->join(
            "project", "kpt.project_id = project.id"
        )->where("kpt.id", $id)->where("kpt.delete_datetime", null)->get();


        if ($query->num_rows() > 0) {
            $result = $query->row_array();
        }

        return $result;
    }

    public function getByProjectId($project_id, $page = 1, $page_items = 10)
    {
        $this->select([
            "id",
            "project_id",
            "title",
            "kpt_datetime"
        ])->where("project_id", $project_id)->where("delete_datetime", null);
        $this->order_by("kpt_datetime", "desc");
        $this->limit($page_items, $page_items * ($page - 1));

        return $this->get()->result_array();
    }

    public function countByProjectId($project_id)
    {
        $this->where("project_id", $project_id)->where("delete_datetime", null);
        return $this->count_all_results();
    }

    public function add($project_id, $title, $date, $description)
    {
        $this->set("project_id", $project_id);
        $this->set("title", $title);
        $this->set("kpt_datetime", $date);
        $this->set("description", $description);
        $this->set("create_datetime", date("Y-m-d H:i:s"));
        $this->set("update_datetime", date("Y-m-d H:i:s"));

        return $this->insert();
    }

    public function update($date, $title, $description, $id)
    {
        $this->set("kpt_datetime", $date);
        $this->set("title", $title);
        $this->set("description", $description);
        $this->set("update_datetime", date("Y-m-d H:i:s"));
        $this->where("id", $id);

        return $this->db->update($this->table_name);
    }
}
