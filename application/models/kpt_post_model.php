<?php
class Kpt_post_model extends MY_Model
{
    const TYPE_KEEP = 1;

    const TYPE_PROBLEM = 2;

    const TYPE_TRY = 3;

    protected $table_name = "kpt_post";

    public function getById($id)
    {
        $result = null;

        $this->where(['id' => $id]);
        $query = $this->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
        }

        return $result;
    }

    /**
     * KPT get post list
     */
    public function getByKptId($kpt_id)
    {
        $this->where(['kpt_id' => $kpt_id]);
        $this->order_by("id", "asc");

        return $this->get()->result_array();
    }

    /**
     * KPT get keep list
     */
    public function getKeepList($kpt_id)
    {
        $this->where(['kpt_id' => $kpt_id, 'type' => self::TYPE_KEEP]);
        $this->order_by("id", "desc");

        return $this->get()->result_array();
    }

    /**
     * KPT get problem list
     */
    public function getProblemList($kpt_id)
    {
        $this->where(['kpt_id' => $kpt_id, 'type' => self::TYPE_PROBLEM]);
        $this->order_by("id", "desc");

        return $this->get()->result_array();
    }

    /**
     * KPT get try list
     */
    public function getTryList($kpt_id)
    {
        $this->where(['kpt_id' => $kpt_id, 'type' => self::TYPE_TRY]);
        $this->order_by("id", "desc");

        return $this->get()->result_array();
    }

    /**
     * KPT add keep post
     */
    public function addKeep($kpt_id, $author_id, $author_name, $comment)
    {
        $data = [
            'kpt_id' => $kpt_id,
            'author_id' => $author_id,
            'author_name' => $author_name,
            'type' => self::TYPE_KEEP,
            'comment' => $comment,
            'create_datetime' => date("Y-m-d H:i:s")
        ];

        return $this->insert($data);
    }

    /**
     * KPT add problem post
     */
    public function addProblem($kpt_id, $author_id, $author_name, $comment)
    {
        $data = [
            'kpt_id' => $kpt_id,
            'author_id' => $author_id,
            'author_name' => $author_name,
            'type' => self::TYPE_PROBLEM,
            'comment' => $comment,
            'create_datetime' => date("Y-m-d H:i:s")
        ];

        return $this->insert($data);
    }

    /**
     * KPT add try post
     */
    public function addTry($kpt_id, $author_id, $author_name, $comment)
    {
        $data = [
            'kpt_id' => $kpt_id,
            'author_id' => $author_id,
            'author_name' => $author_name,
            'type' => self::TYPE_TRY,
            'comment' => $comment,
            'create_datetime' => date("Y-m-d H:i:s")
        ];

        return $this->insert($data);
    }
}
