<?php
if (!function_exists("print_default")) {
    function print_default($data, $default = "", $escape = true)
    {
        if (!isset($data)) {
            $data = $default;
        }

        echo ($escape) ? htmlspecialchars($data, ENT_QUOTES | ENT_HTML5) : $data;
    }
}

if (!function_exists("print_default_array")) {
    function print_default_array($array, $key, $default = "", $escape = true)
    {
        $data = isset($array[$key]) ? $array[$key] : null;
        print_default($data, $default, $escape);
    }
}
