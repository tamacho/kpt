<?php
class Authentication
{
    public function execute()
    {
        $ci = &get_instance();

        $user_id = $ci->session->userdata('user_id');

        if (empty($user_id)) {
            $user_id = str_replace(
                ".", "", uniqid("", true)
            );

            $ci->session->set_userdata("user_id", $user_id);
        }
    }
}
